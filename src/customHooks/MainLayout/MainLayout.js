import React from "react";

import "./styles.css";

import Header from "../../components/Header";

const MainLayout = (props) => {
  return (
    <div className="mainlayout">
      <Header />
      <div className="main">{props.children}</div>
    </div>
  );
};

export default MainLayout;
