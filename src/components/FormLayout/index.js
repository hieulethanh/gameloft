import React from "react";
import Form from "../common/Form";
import Minion from "../../assets/minion.png";

import "./styles.scss";

const FormLayout = () => {
  return (
    <div className="form-layout">
      <div className="container">
        <div className="wrap">
          <div className="row form-layout-row">
            <div className="col-md-6 minion">
              <img src={Minion} alt="minion" />
            </div>
            <div className="col-md-4">
              <h3 style={{ margin: "0", color: "#2699FB", fontSize: "25px" }}>
                Stay in the Know!
              </h3>
              <p style={{ margin: "0", color: "#2699FB" }}>
                Don't get left behind!
              </p>
              <p style={{ margin: "0", color: "#2699FB" }}>
                Subscribe to our newsletters today
              </p>
              <Form />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormLayout;
