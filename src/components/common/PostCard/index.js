import React from "react";

import "./styles.scss";

import Fb from "../../../assets/fb.png";
import Share from "../../../assets/icon.png";

const PostCard = (props) => {
  return (
    <div className="post-card">
      <img src={props.img} alt="..." className="main-img" />
      <img src={Fb} alt="..." className="icon-card-fb" />
      <img src={Share} alt="..." className="icon-card-share" />
      <p>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
        eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
        voluptua. At vero eos voluptua. At vero eos voluptua. At vero eos
        voluptua. At vero eos voluptua.
      </p>
    </div>
  );
};

export default PostCard;
