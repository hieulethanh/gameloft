import React from "react";

import "./styles.scss";

import download from "../../../assets/download.png";

const ContentCard = (props) => {
  return (
    <div className="content-card">
      <img src={props.img} className="main-img" alt="..." />
      <a href="../../../assets/download.png" download>
        <img src={download} alt="..." className="download" />
      </a>
    </div>
  );
};

export default ContentCard;
