import React from "react";
import Carousel from "react-elastic-carousel";

import GameCard from "../GameCard";

import "./styles.scss";

import IMG from "../../../assets/image/alp9.jpg";
import IMG1 from "../../../assets/image/1.jpg";
import IMG2 from "../../../assets/image/2.jpg";
import IMG3 from "../../../assets/image/3.jpg";
const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 375, itemsToShow: 3, itemsToScroll: 2 },
  { width: 769, itemsToShow: 6 },
];

const GameCardCarousel = () => {
  return (
    <div className="carousel">
      <Carousel breakPoints={breakPoints} pagination={false}>
        <GameCard img={IMG} />
        <GameCard img={IMG1} />
        <GameCard img={IMG2} />
        <GameCard img={IMG3} />
        <GameCard img={IMG1} />
        <GameCard img={IMG3} />
      </Carousel>
    </div>
  );
};

export default GameCardCarousel;
