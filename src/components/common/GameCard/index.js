import React, { useState } from "react";

import "./styles.scss";

const GameCard = (props) => {
  const [boxShadow, setBoxShadow] = useState({ boxShadow: "none" });

  const handleClick = () => () => {
    setBoxShadow({ boxShadow: "inset 0 0 0 0.15em #2699FB" });
  };
  return (
    <div className="game-card">
      <img
        style={boxShadow}
        onClick={handleClick()}
        class="input-game-card"
        src={props.img}
        alt="imgg"
      />
      <h6 className="text-center">Asphalt 9</h6>
    </div>
  );
};

export default GameCard;
