import React from "react";

import "./styles.css";

const Form = () => {
  return (
    <form className="row g-3">
      <div className="col-12">
        <input
          type="text"
          className="form-control"
          id="inputAddress"
          style={{ color: "#2699FB", border: "1px solid #2699FB" }}
          placeholder="Name"
        />
      </div>
      <div className="col-12">
        <input
          type="text"
          style={{ color: "#2699FB", border: "1px solid #2699FB" }}
          className="form-control"
          id="inputAddress"
          placeholder="Email"
        />
      </div>

      <div className="col-12">
        <select
          id="inputState"
          className="form-select"
          style={{ color: "#2699FB", border: "1px solid #2699FB" }}
        >
          <option selected>Country</option>
          <option>...</option>
        </select>
      </div>
      <div className="col-12">
        <select
          id="inputState"
          className="form-select"
          style={{ color: "#2699FB", border: "1px solid #2699FB" }}
        >
          <option selected>Platform</option>
          <option>...</option>
        </select>
      </div>
      <div className="col-12">
        <div className="form-check">
          <input className="form-check-input" type="checkbox" id="gridCheck" />
          <label
            className="form-check-label"
            for="gridCheck"
            style={{ color: "#2699FB" }}
          >
            By signing up, I confirm that I am 13 years old or older. I agree to
            the Gameloft Terms and Conditions and I have read the Privacy
            Policy.
          </label>
        </div>
      </div>
      <div className="col-12">
        <div className="form-check">
          <input className="form-check-input" type="checkbox" id="gridCheck" />
          <label
            className="form-check-label"
            for="gridCheck"
            style={{ color: "#2699FB" }}
          >
            I agree to receive promotional offers relating to all Gameloft games
            and services.
          </label>
        </div>
      </div>
      <div className="col-4"></div>
      <div className="col-4">
        <button type="button" className="btn btn-outline-info">
          Button
        </button>
      </div>
      <div className="col-4"></div>
    </form>
  );
};

export default Form;
