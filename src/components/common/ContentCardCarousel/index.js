import React from "react";
import Carousel from "react-elastic-carousel";

import IMG from "../../../assets/image/alp9.jpg";
import IMG1 from "../../../assets/image/4.jpg";
import IMG2 from "../../../assets/image/5.jpg";
import IMG3 from "../../../assets/image/5.jpg";

import ContentCard from "../ContentCard";

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 375, itemsToShow: 3, itemsToScroll: 2 },
  { width: 769, itemsToShow: 3 },
];

const ContentCarousel = () => {
  return (
    <div className="carousel">
      <Carousel breakPoints={breakPoints} showArrows={false}>
        <ContentCard img={IMG} />
        <ContentCard img={IMG1} />
        <ContentCard img={IMG2} />
        <ContentCard img={IMG3} />
      </Carousel>
    </div>
  );
};

export default ContentCarousel;
