import React from "react";

import "./styles.scss";

import Share from "../../../assets/icon.png";

const PromotionalCard = (props) => {
  return (
    <div className="promotional-card">
      <div className="img__img">
        <img src={props.img} alt="..." />
        <h4>Asphalt 9</h4>
      </div>
      <div className="img__description">
        <img src={Share} alt="..." className="icon-card-share" />
        <h5 className="pt-2 pl-3">Asphalt 9</h5>
        <hr />
        <p className="text-center">
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
          nonumy eirmod tempor invidunt ut labore et dolore ...{" "}
        </p>
        <button type="button" className="btn btn-outline-info">
          Read more
        </button>
      </div>
    </div>
  );
};

export default PromotionalCard;
