import React from "react";
import Carousel from "react-elastic-carousel";

import IMG from "../../../assets/image/alp9.jpg";
import IMG1 from "../../../assets/image/1.jpg";
import IMG2 from "../../../assets/image/2.jpg";
import IMG3 from "../../../assets/image/3.jpg";
import PostCard from "../PostCard";

import "./styles.scss";

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 375, itemsToShow: 3, itemsToScroll: 2 },
  { width: 769, itemsToShow: 6 },
];

const PostCardCarousel = () => {
  return (
    <div className="post-card-carousel">
      <Carousel breakPoints={breakPoints} showArrows={false}>
        <PostCard img={IMG} />
        <PostCard img={IMG3} />
        <PostCard img={IMG2} />
        <PostCard img={IMG1} />
        <PostCard img={IMG} />
        <PostCard img={IMG2} />
        <PostCard img={IMG1} />
        <PostCard img={IMG3} />
        <PostCard img={IMG} />
        <PostCard img={IMG2} />
        <PostCard img={IMG1} />
        <PostCard img={IMG} />
      </Carousel>
    </div>
  );
};

export default PostCardCarousel;
