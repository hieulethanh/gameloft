import React from "react";
import Carousel from "react-elastic-carousel";

import IMG from "../../../assets/image/alp9.jpg";
import PromotionalCard from "../PromotionalCard";
import IMG1 from "../../../assets/image/1.jpg";
import IMG2 from "../../../assets/image/2.jpg";
import IMG3 from "../../../assets/image/3.jpg";

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 375, itemsToShow: 3, itemsToScroll: 2 },
  { width: 769, itemsToShow: 4 },
];

const PromotionalCardCarousel = () => {
  return (
    <div className="post-card-carousel">
      <Carousel breakPoints={breakPoints} showArrows={false}>
        <PromotionalCard img={IMG} />
        <PromotionalCard img={IMG1} />
        <PromotionalCard img={IMG2} />
        <PromotionalCard img={IMG3} />
        <PromotionalCard img={IMG1} />
        <PromotionalCard img={IMG2} />
        <PromotionalCard img={IMG3} />
        <PromotionalCard img={IMG1} />
        <PromotionalCard img={IMG2} />
        <PromotionalCard img={IMG3} />
        <PromotionalCard img={IMG2} />
        <PromotionalCard img={IMG1} />
        <PromotionalCard img={IMG} />
        <PromotionalCard img={IMG} />
      </Carousel>
    </div>
  );
};

export default PromotionalCardCarousel;
