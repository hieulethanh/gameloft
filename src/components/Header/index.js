import React from "react";

import Logo from "../../assets/logo.png";
import Hamburger from "../../assets/hamburger.png";
import "./styles.css";

const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg">
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          <img
            src={Logo}
            alt="logo"
            height={72}
            style={{ paddingLeft: "80px" }}
          />
        </a>
        <div className="d-flex">
          <img
            src={Hamburger}
            alt="icon"
            height={21}
            style={{ paddingRight: "81px" }}
          />
        </div>
      </div>
    </nav>
  );
};

export default Header;
