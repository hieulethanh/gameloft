import React from "react";

import Footer from "./layout/Footer";
import MainBody from "./layout/MainBody";

import "fullpage.js/vendors/scrolloverflow";
import ReactFullpage from "@fullpage/react-fullpage";
import MainLayout from "./customHooks/MainLayout/MainLayout";
import FormLayout from "./components/FormLayout";

import GameHub from "./layout/GameHub";
import PromotionalSection from "./layout/PromotionalSection";
import ContentSection from "./layout/ContentSection";

class App extends React.Component {
  onLeave(origin, destination, direction) {
    console.log("Leaving section " + origin.index);
  }
  afterLoad(origin, destination, direction) {
    console.log("After load: " + destination.index);
  }
  render() {
    return (
      <ReactFullpage
        scrollOverflow={true}
        onLeave={this.onLeave.bind(this)}
        afterLoad={this.afterLoad.bind(this)}
        render={({ state, fullpageApi }) => {
          return (
            <div id="fullpage-wrapper">
              <div className="section section1">
                <MainLayout>
                  <MainBody />
                </MainLayout>
              </div>
              <div className="section">
                <FormLayout />
              </div>
              <div className="section">
                <GameHub />
              </div>
              <div className="section">
                <PromotionalSection />
              </div>
              <div className="section">
                <ContentSection />
              </div>
              <div className="section">
                <div className="slide">
                  <Footer />
                </div>
              </div>
            </div>
          );
        }}
      />
    );
  }
}

export default App;
