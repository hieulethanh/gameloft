import React from "react";

import GameCardCarousel from "../../components/common/GameCardCarousel";
import twitter from "../../assets/twitter.png";
import linkedin from "../../assets/linkedin.png";
import fb from "../../assets/fb.png";

import "./styles.scss";
import PostCardCarousel from "../../components/common/PostCardCarousel";

const GameHub = () => {
  return (
    <div className="gamehub ">
      <div className="container">
        <div className="row">
          <h3>Game Community Hub</h3>
          <p>Live game updates</p>
          <GameCardCarousel />
        </div>
        <div className="row pt-3 pb-3">
          <div className="col-6 col-sm-6">
            <h4 style={{ color: "#2699FB" }}>All Posts</h4>
          </div>
          <div className="col-2 col-sm-2">
            <img width={40} height={40} src={linkedin} alt="logo" />
            <img src={twitter} width={40} height={40} alt="logo" />
            <img src={fb} width={40} height={40} alt="logo" />
          </div>
          <div className="col-3">
            <input type="text" class="form-control" placeholder="Search" />
          </div>
        </div>
      </div>
      <PostCardCarousel />
    </div>
  );
};

export default GameHub;
