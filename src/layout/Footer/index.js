import React from "react";

import gameloft from "../../assets/logo.png";
import ytb from "../../assets/ytb.png";
import twitter from "../../assets/twitter.png";
import linkedin from "../../assets/linkedin.png";
import fb from "../../assets/fb.png";

import "./styles.css";

const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md-12 empty"></div>
          <div className="col-md-6 col-sm-12">
            <div className="col-md-12">
              <img src={gameloft} alt="gameloft" width={300} />
            </div>
            <div className="col-md-12">
              <h4 style={{ color: "white" }}>Follow us:</h4>
              <br />
              <div className="row">
                <div className="col">
                  <a href="https://www.facebook.com/">
                    <img width={53} height={53} src={fb} alt="logo" />
                  </a>
                </div>
                <div className="col">
                  <a href="https://www.linkedin.com/">
                    {" "}
                    <img src={linkedin} width={53} height={53} alt="logo" />
                  </a>
                </div>
                <div className="col">
                  <a href="https://twitter.com/home">
                    {" "}
                    <img src={twitter} width={53} height={53} alt="logo" />
                  </a>
                </div>
                <div className="col">
                  <a href="https://www.youtube.com/">
                    <img src={ytb} width={53} height={53} alt="logo" />
                  </a>
                </div>
              </div>
              <div class="col-md-2 pt-5">
                <select id="inputState" class="form-select">
                  <option selected>English</option>
                  <option>Vietnamese</option>
                </select>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 content">
            <div className="row">
              <div className="col-md-6 col-sm-6" style={{ color: "white" }}>
                <ul className="ul-list" style={{ listStyle: "none" }}>
                  <h6>VISIT</h6>

                  <li>Gameloft Game</li>
                  <li>Gameloft Game</li>
                  <li>Gameloft Game</li>
                  <li>Gameloft Game</li>
                  <li>Gameloft Game</li>
                  <li>Gameloft Game</li>
                </ul>
              </div>
              <br />
              <div className="col-md-6 col-sm-6" style={{ color: "white" }}>
                <ul className="ul-list" style={{ listStyle: "none" }}>
                  <h6>LEGAL</h6>

                  <li>Terms and use</li>
                  <li>Privacy policy</li>
                  <li>Cookie policy</li>
                  <li>EULA</li>
                  <li>Event Rule</li>
                  <li>Business contacts</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="br" style={{ marginTop: "100px" }}>
        <hr height={10} />
      </div>
      <div class="d-flex justify-content-center" style={{ color: "white" }}>
        <p style={{ alignContent: "center", padding: "30px" }}>
          Name: ©2020 Gameloft. All rights reserved. Gameloft and the Gameloft
          logo are trademarks of Gameloft in the U.S. and/or other countries.
          <br /> All other trademarks are the property of their respective
          owners.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
