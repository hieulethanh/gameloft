import React from "react";

import Image from "../../assets/mountain.png";
import Rating from "../../components/common/Rating";
import Share from "../../assets/share.png";
import Steam from "../../assets/steam.png";
import Nintendo from "../../assets/nintendo.png";
import Ms from "../../assets/ms.png";
import "./styles.css";

const MainBody = () => {
  return (
    <div className="main-body">
      <div className="container">
        <div className="wrap">
          <img
            src={Image}
            alt="Logo"
            className="rounded mx-auto d-block img-mountain"
          />
        </div>
        <div className="content">
          <h3>Gameloft Game</h3>
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                <p>Rating/Action |</p> <Rating />
              </div>
            </div>
            <p className="sub-content">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut
              aliquip ex ea commodo consequat. Duis aute irure dolor i
            </p>
          </div>
        </div>
        <div className="share">
          <img src={Share} alt="share" />
        </div>
      </div>
      <div className="footer-main-body">
        <div className="container">
          <div className="row">
            <div
              className="col-md-7 col-sm-12"
              style={{ color: "#2699FB", fontSize: "20px", fontWeight: "600" }}
            >
              Download lastest version
            </div>
            <div className="col-md-5 col-sm-12">
              <img src={Nintendo} alt="nintendo" style={{ padding: "5px" }} />
              <img src={Ms} alt="ms" style={{ padding: "5px" }} />
              <img src={Steam} alt="steam" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainBody;
