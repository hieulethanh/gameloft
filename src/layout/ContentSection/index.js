import React from "react";

import "./styles.scss";

import ContentCardCarousel from "../../components/common/ContentCardCarousel";

const ContentSection = () => {
  return (
    <div className="content-section">
      <div className="container pt-4 mb-5">
        <div className="row">
          <h4 className="pt-3">Exclusive Game Content</h4>
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos.{" "}
          </p>
        </div>
      </div>
      <ContentCardCarousel />
    </div>
  );
};

export default ContentSection;
