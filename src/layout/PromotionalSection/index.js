import React from "react";

import PromotionalCardCarousel from "../../components/common/PromotionalCardCarousel";

import "./styles.scss";

const PromotionalSection = () => {
  return (
    <div className="promotional-section">
      <div className="container pt-4 mb-5">
        <div className="row">
          <h4 className="pt-3">Special Events & Promotional</h4>
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos.
          </p>
        </div>
      </div>
      <PromotionalCardCarousel />
    </div>
  );
};

export default PromotionalSection;
